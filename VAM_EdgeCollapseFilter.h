///
/// @file VAM_EdgeCollapseFilter.h
/// @author Michal Zak
/// @brief VAM_EdgeCollapseFilter class, filter implementing edge collapse (decimating mesh).
///

#pragma once
#include "vam_meshemptyfilter.h"
#include "PriorityQueue.h"

// ------------------------------------------------------------------------------
/// Filter implementing edge collapse (decimating mesh).
//
// Warning - we use only positions and faces, edges in structures are not used nor updated during process
class VAM_EdgeCollapseFilter : public VAM_MeshEmptyFilter
{
private:
  std::vector<std::vector<int> > mergeList; ///< in each edge collapse, we remember which all vertices are collapsed in one

  CPriorityQueue<double, int> queue; ///< priority queue
  
  std::vector<Eigen::Matrix4d> vertMatrixQ; ///< shape cost matrix for each vertex
  std::vector<int>             vertMinErrIndex; ///< array of least significant edges - from [i] to v[i]

  /// Compute shape cost matrix for vertex[index].
  void ComputeVertMatrixQ(const int index);
  /// Compute priority of vertex edge (determined by its least valuable edge).
  double ComputeVertexPriority(const int index);

  /// Move vertices of skeleton to centroids of original vertex groups.
  void MoveVertsToCentroids();

protected:
  VAM_EdgeCollapseFilter(void);
  ~VAM_EdgeCollapseFilter(void);

  /// Copy data from internal structures.
  virtual void DoneMesh(vtkPolyData* toPolydata);
  /// Execution of edge collapse filter.
  virtual void Execute();
public:
  std::vector<CVertex>* originalVertexes; ///< pointer to vertexes before Laplace transform

  double weightShapeCost;       ///< determines penalizing collapse with "wrong shape", default = 1 
  double weightSamplingCost;    ///< determines penalizing collapse with long edges, default = 0.1

  /// VTK macro.
  vtkTypeMacro(VAM_EdgeCollapseFilter, VAM_MeshEmptyFilter);
  /// Print info about this object.
  void PrintSelf(ostream& os, vtkIndent indent);
  /// Create new instance.
  static VAM_EdgeCollapseFilter *New();
};


// ------------------------------------------------------------------------------