#include "stdafx.h"
#include "VAM_MeshLaplaceFilter.h"
#include "VAM_EdgeCollapseFilter.h"
 
/// Entry point of the program.
int main ( int argc, char *argv[] )
{
  // help (no arguments given)
  if(argc < 2 || argc % 2 != 0)
  {
    std::cout << "Usage: " << argv[0] << "  Filename(.obj) [options]" << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << " itercount N       iteration limit" << std::endl;
    std::cout << " poscoeff D        initial coeff for positions " << std::endl;
    std::cout << " areacoeff D       initial coeff for areas " << std::endl;
    std::cout << " sl D              multiplying coeff for Laplacian" << std::endl;
    std::cout << " volumecoeff D     stop when volume is 1/Dx smaller than initial" << std::endl;
    std::cout << "Example:" << std::endl;
    std::cout << " " << argv[0] << " model.obj itercount 5 poscoeff 0.5 areacoeff 1 sl 2 volumecoeff 0.0001" << std::endl;
    return EXIT_FAILURE;
  }
 
  // parsing command line
  std::string inputFilename = argv[1];
  int maxitercount = 10;
  double poscoeff = 1.0;
  double areacoef = 1.0;
  double sL = 2.0;
  double volumeCoef = 1e-5;
  double keep_faces_perces = 0;
  for (int i = 2; i < argc; i+=2)
  {
    if (!strcmp(argv[i], "itercount"))
      maxitercount = strtol(argv[i+1], NULL, 10);
    else if (!strcmp(argv[i], "poscoeff"))
      poscoeff = strtod(argv[i+1], NULL);
    else if (!strcmp(argv[i], "areacoeff"))
      areacoef = strtod(argv[i+1], NULL);
    else if (!strcmp(argv[i], "sl"))
      sL = strtod(argv[i+1], NULL);
    else if (!strcmp(argv[i], "volumecoeff"))
      volumeCoef = strtod(argv[i+1], NULL);
    else if (!strcmp(argv[i], "keepfaces"))
      keep_faces_perces = strtod(argv[i+1], NULL);
    else 
    {
      std::cout << "Unknown option " << argv[i] << std::endl;
      return EXIT_FAILURE;
    }
  }

  vtkSmartPointer<vtkOBJReader> reader = vtkSmartPointer<vtkOBJReader>::New();
  reader->SetFileName ( inputFilename.c_str() );
 
  // Filter #1: Laplacian transformation
  vtkSmartPointer<VAM_MeshLaplaceFilter> laplaceTransform =
    vtkSmartPointer<VAM_MeshLaplaceFilter>::New();
  laplaceTransform->SetConstants(maxitercount, sL, areacoef, volumeCoef, poscoeff);
  laplaceTransform->SetInputConnection(reader->GetOutputPort());

  // Filter #2: Edge collapse
  vtkSmartPointer<VAM_EdgeCollapseFilter> edgeCollapse =
    vtkSmartPointer<VAM_EdgeCollapseFilter>::New();
  edgeCollapse->originalVertexes = &laplaceTransform->originalVertexes;
  edgeCollapse->SetInputConnection(laplaceTransform->GetOutputPort());

  // Visualize loaded model
  vtkSmartPointer<vtkPolyDataMapper> mapperModel =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperModel->SetInputConnection(reader->GetOutputPort());

  vtkSmartPointer<vtkActor> actorModel =
    vtkSmartPointer<vtkActor>::New();
  actorModel->SetMapper(mapperModel);
  actorModel->GetProperty()->SetOpacity(0.25);

  // Visualize Laplace transform
  vtkSmartPointer<vtkPolyDataMapper> mapperLaplace =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperLaplace->SetInputConnection(laplaceTransform->GetOutputPort());

  vtkSmartPointer<vtkActor> actor =
    vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapperLaplace);
  actor->GetProperty()->SetOpacity(0.15);
  actor->GetProperty()->SetColor(0.0, 1.0, 0.0);

  // Visualize skeleton
  vtkSmartPointer<vtkPolyDataMapper> mapper =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputConnection(edgeCollapse->GetOutputPort());

  vtkSmartPointer<vtkActor> actorSkeleton =
    vtkSmartPointer<vtkActor>::New();
  actorSkeleton->SetMapper(mapper);
  actorSkeleton->GetProperty()->SetRepresentationToWireframe();
  actorSkeleton->GetProperty()->SetColor(0.0, 0.0, 0.0);
  actorSkeleton->GetProperty()->LightingOff();
 
  vtkSmartPointer<vtkRenderer> renderer =
    vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renderWindow =
    vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->SetSize(640, 480);
  renderWindow->Render();

  renderWindow->AddRenderer(renderer);
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);
 
  renderer->AddActor(actor);
  renderer->AddActor(actorModel);
  renderer->AddActor(actorSkeleton);
  renderer->SetBackground(1, 1, 1);
 
  renderWindow->Render();
  renderWindowInteractor->Start();
  char windowTitle[1024];
  sprintf(windowTitle, "%s - %s, iter: %d", argv[0], argv[1], maxitercount);
  renderWindow->SetWindowInfo(windowTitle);
 
  return EXIT_SUCCESS;
}