///
/// @file VAM_MeshLaplaceFilter.cpp
/// @author Michal Zak
/// @brief VAM_MeshLaplaceFilter class, implementation of weighted iterative Laplace transformation.
///

#include "stdafx.h"
#include "VAM_MeshLaplaceFilter.h"

// ------------------------------------------------------------------------------
VAM_MeshLaplaceFilter::VAM_MeshLaplaceFilter(void)
{
  IterationCount = 10;
  sL = 2;
  initialWHCoef = 1;
  volumeCoef = 1e-6;
  avgFaceAreaCoef = 10;
  loggingEnabled = true;
}

// ------------------------------------------------------------------------------
VAM_MeshLaplaceFilter::~VAM_MeshLaplaceFilter(void)
{
}

// ------------------------------------------------------------------------------
void VAM_MeshLaplaceFilter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

// ------------------------------------------------------------------------------
void VAM_MeshLaplaceFilter::Execute()
{
  originalVertexes = vertexes;
  faceArea.resize(numberTriangles);
  faceAreaInitial.resize(numberTriangles);

  matrixW_H.resize(numberVertexes);
  matrixW_L.resize(numberVertexes);
  matrixL.resize(numberVertexes, numberVertexes);
  matrixL.reserve(numberEdges * 2 + numberVertexes);

  const double initialVolume = ComputeVolume();
  if (loggingEnabled)
  {
    printf("Current mesh volume        : %lf\n", initialVolume);
    printf("             boundbox size : %lf x %lf x %lf\n", aabb.maxx - aabb.minx, aabb.maxy - aabb.miny, aabb.maxz - aabb.minz);
  }

  for (int iter = 0; iter < IterationCount; iter++)
  {
    if (loggingEnabled)
    {
      printf("Laplace filter: iteration %d started\n", iter + 1);
      printf("Laplace filter: computing face areas...\n", iter + 1);
    }
    ComputeFaceAreas(iter == 0);

    if (loggingEnabled)
      printf("Laplace filter: computing new weights...\n", iter + 1);
    ComputeWeights(iter == 0);
    if (loggingEnabled)
      printf("Laplace filter: computing Laplace operator...\n", iter + 1);
    ComputeLaplaceOperator();
    if (loggingEnabled)
      printf("Laplace filter: computing new vertex positions...\n", iter + 1);

    ComputeNewVertexes();

    if (loggingEnabled)
      printf("Laplace filter: iteration %d finished\n", iter + 1);

    const double currentVolume = ComputeVolume();
    if (loggingEnabled)
      printf("Current mesh volume is %lf\n", currentVolume);
    if (initialVolume * volumeCoef > currentVolume)
    {
      if (loggingEnabled)
      {
        printf("Stopping process, mesh volume is now small enough.\n");
        break;
      }
    }

  }

  faceArea.clear();
  faceAreaInitial.clear();
}

// ------------------------------------------------------------------------------
/// Compute area of each face.
void VAM_MeshLaplaceFilter::ComputeFaceAreas(bool initial)
{
  if (!numberTriangles)
    return;
  vtkstd::vector<CTriangle>::const_pointer triangle = &triangles[0];
  for (int i = 0; i < numberTriangles; i++, triangle++)
  {
    faceArea[i] = ComputeArea(vertexes[triangle->aVertex[0]], vertexes[triangle->aVertex[1]], vertexes[triangle->aVertex[2]]);
  }

  if (initial)
    faceAreaInitial = faceArea;
}

// ------------------------------------------------------------------------------
/// Compute laplace operator "matrixL".
void VAM_MeshLaplaceFilter::ComputeLaplaceOperator()
{
  if (!numberTriangles)
    return;
  vtkstd::vector<CEdge>::const_pointer e = &(edges[0]);
  double* rowSums = new double[numberVertexes];
  for (int i = 0; i < numberVertexes; i++)
  {
    rowSums[i] = 0.0;
  }

  int numLogInfoCounter = 10000;
  for (int ei = 0; ei < numberEdges; ei++, e++)
  {
    if (loggingEnabled)
    {      
      if (numLogInfoCounter == 0)
      {
        printf("Laplace filter: computing Laplace op.... (edge %d/%d)             \r", ei, numberEdges);
        numLogInfoCounter = 10000;
      }
      numLogInfoCounter--;
    }

    int i = e->aVertex[0];
    int j = e->aVertex[1];
    int left = e->aVertex[2];
    int right = e->aVertex[3];
    double alfa = (left >= 0) ? ComputeAngle(vertexes[i], vertexes[left], vertexes[j]) : M_PI;
    double beta = (right >= 0) ? ComputeAngle(vertexes[i], vertexes[right], vertexes[j]) : M_PI;
    double weight = 1.0 / tan(alfa) + 1.0 / tan(beta);
    matrixL.coeffRef(i, j) = matrixL.coeffRef(j, i) = weight;
    rowSums[i] += weight;
    rowSums[j] += weight;
  }
  printf("Laplace filter: computing Laplace op... (edge %d/%d)                 \n", numberEdges, numberEdges);

  for (int i = 0; i < numberVertexes; i++)
  {
    matrixL.coeffRef(i, i) = -rowSums[i];    
  }

  matrixL.makeCompressed();

  delete[] rowSums;
}

// ------------------------------------------------------------------------------
/// Compute weight matrices "matrixW_L" nad "matrixW_H".
void VAM_MeshLaplaceFilter::ComputeWeights(bool initial)
{
  if (!numberTriangles)
    return;
  if (initial)
  {        
    double averageFaceArea = 0.0;
    for (int i = 0; i < numberTriangles; i++)
      averageFaceArea += faceAreaInitial[i];
    //averageFaceArea /= numberTriangles;

    matrixW_H.setConstant(initialWHCoef);
    matrixW_L.setConstant(avgFaceAreaCoef * sqrt(averageFaceArea));
  }
  else
  {
    matrixW_L *= sL;

    vtkstd::vector<CVertex>::const_pointer vertex = &(vertexes[0]);    
    for (int i = 0; i < numberVertexes; i++, vertex++)
    {
      const int countRingTriangles = vertex->OneRingTriangle.size();
      double areaStart = 0.0;
      double areaAfter = 0.0;
      for (int j = 0; j < countRingTriangles; j++)
      {
        areaStart += faceAreaInitial[vertex->OneRingTriangle[j]];
        areaAfter += faceArea[vertex->OneRingTriangle[j]];
      }

      double backup = matrixW_H[i];
      matrixW_H[i] = initialWHCoef * sqrt(areaStart / areaAfter);
      if (matrixW_H[i] != matrixW_H[i])
        matrixW_H[i] = backup;
    }
  }
}

// ------------------------------------------------------------------------------
/// Compute new vertex positions.
void VAM_MeshLaplaceFilter::ComputeNewVertexes()
{
  if (!numberTriangles)
    return;
  Eigen::SparseMatrix<double> matrixA;  
  matrixA.resize(numberVertexes * 2, numberVertexes);
  matrixA.reserve(numberEdges * 2 + numberVertexes * 2);    
  for (int k = 0; k < matrixL.outerSize(); k++)
    for (Eigen::SparseMatrix<double>::InnerIterator it(matrixL, k); it; ++it)
    {      
      const int row = it.row();      
      matrixA.coeffRef(row, it.col()) = matrixW_L[row] * it.value();        
    }

  for (int k = 0; k < numberVertexes; k++)
    matrixA.coeffRef(numberVertexes + k, k) = matrixW_H.coeff(k);

  Eigen::VectorXd rhs_x;
  Eigen::VectorXd rhs_y;
  Eigen::VectorXd rhs_z;
  rhs_x.resize(numberVertexes * 2);
  rhs_y.resize(numberVertexes * 2);
  rhs_z.resize(numberVertexes * 2);
  for (int k = 0; k < numberVertexes; k++)
  {
    rhs_x[k] = vertexes[k].dCoord[0];
    rhs_y[k] = vertexes[k].dCoord[1];
    rhs_z[k] = vertexes[k].dCoord[2];
    rhs_x[numberVertexes+k] = matrixW_H[k] * vertexes[k].dCoord[0];
    rhs_y[numberVertexes+k] = matrixW_H[k] * vertexes[k].dCoord[1];
    rhs_z[numberVertexes+k] = matrixW_H[k] * vertexes[k].dCoord[2];
  }

  rhs_x = matrixA.transpose() * rhs_x;
  rhs_y = matrixA.transpose() * rhs_y;
  rhs_z = matrixA.transpose() * rhs_z;
  matrixA = Eigen::SparseMatrix<double>(matrixA.transpose()) * matrixA;

  Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>> solver;
  solver.compute(matrixA);

  aabb = CBoundBox();
  Eigen::VectorXd newX = solver.solve(rhs_x);
  Eigen::VectorXd newY = solver.solve(rhs_y);
  Eigen::VectorXd newZ = solver.solve(rhs_z);
  for (int k = 0; k < numberVertexes; k++)
    if (newX[k] == newX[k] &&
      newY[k] == newY[k] &&
      newZ[k] == newZ[k])
    {
      vertexes[k].dCoord[0] = newX[k];
      vertexes[k].dCoord[1] = newY[k];
      vertexes[k].dCoord[2] = newZ[k];
      aabb.Join(&vertexes[k]);
    }
}

// ------------------------------------------------------------------------------
/// Compute volume of mesh.
double VAM_MeshLaplaceFilter::ComputeVolume()
{
  if (!numberTriangles)
    return 0;
  const int triagleCount = (int)triangles.size();
  if (triagleCount == 0)
    return 0;

  double volume = 0;

  const double outerCoeff = 0.3;
  double outerPoint[3];
  outerPoint[0] = aabb.minx - (aabb.maxx - aabb.minx) * outerCoeff;
  outerPoint[1] = aabb.miny - (aabb.maxy - aabb.miny) * outerCoeff;
  outerPoint[2] = aabb.minz - (aabb.maxz - aabb.minz) * outerCoeff;

  CTriangle* pTriangle = &(triangles[0]);
  for (int i = 0; i < triagleCount; i++, pTriangle++)
  {
    const int iv0 = pTriangle->aVertex[0];
    const int iv1 = pTriangle->aVertex[1];
    const int iv2 = pTriangle->aVertex[2];
    const CVertex& v0 = vertexes[iv0];
    const CVertex& v1 = vertexes[iv1];
    const CVertex& v2 = vertexes[iv2];

    Eigen::Vector3d e1(v1.dCoord[0] - v0.dCoord[0], v1.dCoord[1] - v0.dCoord[1], v1.dCoord[2] - v0.dCoord[2]);
    Eigen::Vector3d e2(v2.dCoord[0] - v0.dCoord[0], v2.dCoord[1] - v0.dCoord[1], v2.dCoord[2] - v0.dCoord[2]);
    Eigen::Vector3d toOuterPoint(outerPoint[0] - v0.dCoord[0], outerPoint[1] - v0.dCoord[1], outerPoint[2] - v0.dCoord[2]);
    Eigen::Vector3d normal(e2.cross(e1));
    const double volumePart = toOuterPoint.dot(normal);
    volume += volumePart;
  }

  return volume / 6.0;
}

// ------------------------------------------------------------------------------
vtkStandardNewMacro(VAM_MeshLaplaceFilter);