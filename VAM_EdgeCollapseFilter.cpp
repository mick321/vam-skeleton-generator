///
/// @file VAM_EdgeCollapseFilter.cpp
/// @author Michal Zak
/// @brief VAM_EdgeCollapseFilter class, filter implementing edge collapse (decimating mesh).
///

#include "StdAfx.h"
#include "VAM_EdgeCollapseFilter.h"

// ------------------------------------------------------------------------------
template <typename T>
void swap(T& a, T& b)
{
  T tmp = a;
  a = b;
  b = tmp;
}

// ------------------------------------------------------------------------------
template <typename T>
bool isInVector(const std::vector<T>& vec, const T& value)
{
  return std::find(vec.begin(), vec.end(), value) != vec.end();
}

// ------------------------------------------------------------------------------
template <typename T>
bool addToVectorIfMissing(std::vector<T>& vec, const T& value)
{
  if (!isInVector(vec, value))
  {
    vec.push_back(value);
    return true;
  }
  return false;
}

// ------------------------------------------------------------------------------
template <typename T>
bool removeFromVector(std::vector<T>& vec, const T& value)
{
  size_t lastsize = vec.size();
  vec.erase(std::remove(vec.begin(), vec.end(), value), vec.end());
  return lastsize != vec.size();
}

// ------------------------------------------------------------------------------
VAM_EdgeCollapseFilter::VAM_EdgeCollapseFilter(void)
{
  weightShapeCost = 0.1;
  weightSamplingCost = 1;
  originalVertexes = NULL;
}

// ------------------------------------------------------------------------------
VAM_EdgeCollapseFilter::~VAM_EdgeCollapseFilter(void)
{
}

// ------------------------------------------------------------------------------
void VAM_EdgeCollapseFilter::ComputeVertMatrixQ(const int index)
{
  CVertex& v = vertexes[index];
  Eigen::Matrix4d& q = vertMatrixQ[index];
  q.fill(0);
  for (int i : v.OneRingVertex)
  {
    CVertex& v1 = v;
    CVertex& v2 = vertexes[i];

    Eigen::Vector3d a; // orientation of edge
    a.x() = v2.dCoord[0] - v1.dCoord[0];
    a.y() = v2.dCoord[1] - v1.dCoord[1];
    a.z() = v2.dCoord[2] - v1.dCoord[2];
    double alen = a.norm();
    a /= alen;
    Eigen::Vector3d b; // orientation of edge
    b = a.cross(Eigen::Vector3d(v2.dCoord[0], v2.dCoord[1], v2.dCoord[2]));

    Eigen::Matrix4d m;
    m.fill(0);
    m.coeffRef(0, 0) = 0;      m.coeffRef(0, 1) = -a.z(); m.coeffRef(0, 2) = a.y();  m.coeffRef(0, 3) = -b.x();
    m.coeffRef(1, 0) = a.z();  m.coeffRef(1, 1) = 0;      m.coeffRef(1, 2) = -a.x(); m.coeffRef(1, 3) = -b.y();
    m.coeffRef(2, 0) = -a.y(); m.coeffRef(2, 1) = a.x();  m.coeffRef(2, 2) = 0;      m.coeffRef(2, 3) = -b.z();

    q += m.transpose() * m;
  }
}

// ------------------------------------------------------------------------------
double VAM_EdgeCollapseFilter::ComputeVertexPriority(const int index)
{
  CVertex& vert = vertexes[index];
  Eigen::Vector3d p(vert.dCoord[0], vert.dCoord[1], vert.dCoord[2]);
  double minError = DBL_MAX; 
  int minIndex = -1;

  if (vert.OneRingVertex.size() > 1 && vert.OneRingTriangle.size() > 0)
  {
    // compute average length of edges around vert
    double avgLength = 0.0;
    for (int i : vert.OneRingVertex)
    {
      CVertex& vert2 = vertexes[i];
      Eigen::Vector3d p2(vert2.dCoord[0], vert2.dCoord[1], vert2.dCoord[2]);
      avgLength += (p - p2).norm();
    }
    avgLength /= (double)vert.OneRingVertex.size();

    // walk through on-ring vertices around vert
    for (int j : vert.OneRingVertex)
    {
      // we seek reference to (J)
      bool found = false;
      for (int triindex : vert.OneRingTriangle)
      {
        const CTriangle& tri = triangles[triindex];
        if (tri.aVertex[0] == j || tri.aVertex[1] == j || tri.aVertex[2] == j)
        {
          found = true; // here it is!
        }
      }
      
      if (found)
      {
        CVertex& vertexJ = vertexes[j];
        Eigen::Vector3d p2(vertexJ.dCoord[0], vertexJ.dCoord[1], vertexJ.dCoord[2]);
        // does it have any adjacent triangles?
        if (vertexJ.OneRingTriangle.size() != 0)
        {
          double err;
          err = (p - p2).norm() * avgLength; // first part of error
          err *= weightSamplingCost;
          
          Eigen::Vector4d v(p2.x(), p2.y(), p2.z(), 1.0);
          Eigen::Matrix4d matrixQ = vertMatrixQ[index] + vertMatrixQ[j];
          double e = v.dot(matrixQ * v) * weightShapeCost;
          err += e;

          // renew err value
          if (err < minError)
          {
            minError = err;
            minIndex = j;
          }
        }
      }
    }
  }

  vertMinErrIndex[index] = minIndex;
  // a to je cely update vrcholu! (staci jej narvat zase do fronty)
  return minError;
}

// ------------------------------------------------------------------------------
void VAM_EdgeCollapseFilter::MoveVertsToCentroids()
{
  if (!originalVertexes)
    return;
  std::vector<CVertex>& o_verts = *originalVertexes;

  for (size_t i = 0; i < vertexes.size(); i++)
  {
    if (mergeList.size())
    {
      double x = 0, y = 0, z = 0;
      for (int j : mergeList[i])
      {
        x += o_verts[j].dCoord[0];
        y += o_verts[j].dCoord[1];
        z += o_verts[j].dCoord[2];
      }
      double invMergeListSize = 1.0 / mergeList[i].size();
      x *= invMergeListSize;
      y *= invMergeListSize;
      z *= invMergeListSize;
      vertexes[i].dCoord[0] = x;
      vertexes[i].dCoord[1] = y;
      vertexes[i].dCoord[2] = z;
    }
  }
}

// ------------------------------------------------------------------------------
/// Execution of edge collapse filter.
void VAM_EdgeCollapseFilter::Execute()
{
  if (!numberEdges)
    return;

  // 1. prepare the queue
  queue.Clear();  

  vertMatrixQ.clear();
  mergeList.clear();
  vertMinErrIndex.clear();

  vertMatrixQ.resize(numberVertexes);
  mergeList.resize(numberVertexes);
  vertMinErrIndex.resize(numberVertexes);
  
  for (int i = 0; i < numberVertexes; i++)
  {
    ComputeVertMatrixQ(i);
    mergeList[i].push_back(i);
  }

  for (int i = 0; i < numberVertexes; i++)
  {
    double priority = ComputeVertexPriority(i);
    queue.Put(priority, i);
  }

  int facesLeft = (int)triangles.size();
  // 2. do the edge collapse
  while (facesLeft > 0 && !queue.IsEmpty())
  {
    double priority;
    int vertexIndex1 = queue.Get(priority);
    int vertexIndex2 = vertMinErrIndex[vertexIndex1];
    CVertex& v1 = vertexes[vertexIndex1];
    CVertex& v2 = vertexes[vertexIndex2];
    
    vertMatrixQ[vertexIndex2] += vertMatrixQ[vertexIndex1];

    // merge vertexIndex1->vertexIndex2
    mergeList[vertexIndex2].insert(mergeList[vertexIndex2].end(), mergeList[vertexIndex1].begin(), mergeList[vertexIndex1].end());
    mergeList[vertexIndex1].clear();

    int count = 0;
    // walk through one-ring faces around V1
    for (int faceIndex : v1.OneRingTriangle)
    {
      CTriangle& triangle = triangles[faceIndex];
      int& tv1 = triangle.aVertex[0];
      int& tv2 = triangle.aVertex[1];
      int& tv3 = triangle.aVertex[2];
      if (tv1 == vertexIndex2 || tv2 == vertexIndex2 || tv3 == vertexIndex2 || 
        tv1 == tv2 || tv2 == tv3 || tv1 == tv3)
      {
        // we remove all triangles which are degenerated or contain V2
        for (int vi : v1.OneRingVertex)
        {
          // so we remove references
          auto& oneRingF = vertexes[vi].OneRingTriangle;
          removeFromVector(oneRingF, faceIndex);
        }
        // one triangle was deleted
        facesLeft--; 
        count++; 
      }
      else
      {
        // give triangle to V2
        if (tv1 == vertexIndex1)
          tv1 = vertexIndex2;
        if (tv2 == vertexIndex1)
          tv2 = vertexIndex2;
        if (tv3 == vertexIndex1)
          tv3 = vertexIndex2;

        addToVectorIfMissing(v2.OneRingTriangle, faceIndex);
      }
    }
    // walk through one-ring vertices around V1
    for (int vertAdjIndex : v1.OneRingVertex)
    {
      CVertex& vertAdj = vertexes[vertAdjIndex];
      // delete reference to V1
      removeFromVector(vertAdj.OneRingVertex, vertexIndex1);
      
      // connect vertAdj to v2
      if (vertAdjIndex != vertexIndex2)
      {
        addToVectorIfMissing(vertAdj.OneRingVertex, vertexIndex2);
        addToVectorIfMissing(v2.OneRingVertex, vertAdjIndex);
      }
    }
    // walk through one-ring vertices around V2
    for (int vertAdjIndex : v2.OneRingVertex)
    {
      double priority = ComputeVertexPriority(vertAdjIndex);
      queue.ModifyPriority(vertAdjIndex, priority);
    }
    // update V2
    {
      double priority = ComputeVertexPriority(vertexIndex2);
      queue.ModifyPriority(vertexIndex2, priority);
    }

    // walk through one-ring faces around V2
    for (int faceIndex : v2.OneRingTriangle)
    {
      CTriangle& triangle = triangles[faceIndex];
      int& tv1 = triangle.aVertex[0];
      int& tv2 = triangle.aVertex[1];
      int& tv3 = triangle.aVertex[2];
      // remove degenerated triangles
      if (tv1 == tv2 || tv1 == tv3 || tv2 == tv3)
      {
        removeFromVector(v2.OneRingTriangle, faceIndex);
        for (int vertIndexNeighborV2 : v2.OneRingVertex)
        {
          removeFromVector(vertexes[vertIndexNeighborV2].OneRingTriangle, faceIndex);
        }
        facesLeft--; // one triangle deleted
      }
    }

    if (count == 0)
    {
      printf("???");
    }
    v1.OneRingVertex.clear();
    v1.OneRingTriangle.clear();
  }
  
  // 3. refinement
  MoveVertsToCentroids();
}

// ------------------------------------------------------------------------------
/// Copy data from internal structures.
void VAM_EdgeCollapseFilter::DoneMesh(vtkPolyData* toPolydata)
{
  if (!numberTriangles)
    return;

  // Setup two colors - one for each line
  unsigned char blue[3] = {0, 0, 255};
  unsigned char white[3] = {50, 50, 255};
 
  // Setup the colors array
  vtkSmartPointer<vtkUnsignedCharArray> colors =
    vtkSmartPointer<vtkUnsignedCharArray>::New();
  colors->SetNumberOfComponents(3);
  colors->SetName("Colors");


  int i;
  // set up polydata object and data arrays
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();  

  vtkstd::vector<CVertex>::const_pointer	  vertex;  
  vertex = &(vertexes[0]);
  points->Allocate(numberVertexes);
  for(i = 0; i < numberVertexes; i++, vertex++)
  {
    points->SetPoint(i, vertex->dCoord[0], vertex->dCoord[1], vertex->dCoord[2]);    
  }
  
  toPolydata->SetPoints(points);

  toPolydata->Allocate(numberVertexes);
  for (i = 0; i < numberVertexes; i++)
  {
    CVertex& vert = vertexes[i];
    for (int j : vert.OneRingVertex)
      if (j > i)
      {
        vtkIdType pts[2];
        pts[0] = (vtkIdType)i;
        pts[1] = (vtkIdType)j;

        toPolydata->InsertNextCell(VTK_LINE, 2, pts);
        colors->InsertNextTupleValue(white);
      }
  }
  
  toPolydata->GetCellData()->SetScalars(colors);

  vertexes.clear();
  triangles.clear();
  edges.clear();

  numberVertexes = 0;
  numberTriangles = 0;
  numberEdges = 0;
}

// ------------------------------------------------------------------------------
void VAM_EdgeCollapseFilter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

// ------------------------------------------------------------------------------
vtkStandardNewMacro(VAM_EdgeCollapseFilter);