///
/// @file VAM_MeshEmptyFilter.h
/// @author Michal Zak
/// @brief VAM_MeshEmptyFilter class, empty filter with internal structures
///

#pragma once
#include "vtkpolydataalgorithm.h"
#include <algorithm>

/// Empty filter - builds internal structures and writes them back.
/// Inherit this class to override structure creation / execution / write back methods.
class VAM_MeshEmptyFilter : public vtkPolyDataAlgorithm
{
public:
  //----------------------------------------------------------------------------
  /** Nested Vertex class 
  This is a list of the triangles, edges and vertices which are joined to this vertex.*/
  //----------------------------------------------------------------------------
  class CVertex
  {
  public:
    double  dCoord[3];
    int     degree;

    vtkstd::vector<int> OneRingTriangle;
    vtkstd::vector<int> TwoRingVertex;

    vtkstd::vector<int> OneRingEdge;
    vtkstd::vector<int> OneRingVertex;

    CVertex();
    ~CVertex();
    void SetCoord(double *pCoord);
  };

  //----------------------------------------------------------------------------
  /** Nested Triangle class 
  Each triangle has three edges and three vertices.*/
  //----------------------------------------------------------------------------
  class  CTriangle
  {
  public:
    int   aVertex[3];
    int   aEdge[3];
  public:
    CTriangle();
    void SetVertex(int* v);
    void SetVertex(int v0,int v1,int v2);
  };

  //----------------------------------------------------------------------------
  /** Nested Edge class 
  Normally, each edge has two neighbor triangles. Two vertices consist of an edge.*/
  //----------------------------------------------------------------------------
  class CEdge
  {
  public:
    int   aVertex[4];       //first second, left right
    int   aTriangle[2];     //left right
    bool  bBoundary;
    CEdge(); 
    void SetTriangle(int t0,int t1);
    void SetVertex(int v0,int v1,int v2,int v3);
  };

  //----------------------------------------------------------------------------
  /** Axis oriented bound box.*/
  //----------------------------------------------------------------------------
  class CBoundBox
  {
  public:
    double minx, miny, minz;
    double maxx, maxy, maxz;
    CBoundBox();
    void Join(CVertex* v);
  };

private:  
  /// Not implemented.
  VAM_MeshEmptyFilter(const VAM_MeshEmptyFilter&) : vtkPolyDataAlgorithm() { }
  /// Not implemented.
  void operator=(const VAM_MeshEmptyFilter&) { }

protected:

  int numberEdges;
  int numberVertexes;
  int numberTriangles;
  vtkstd::vector<CVertex>     vertexes;
  vtkstd::vector<CTriangle>   triangles;
  vtkstd::vector<CEdge>       edges;
  CBoundBox aabb;

  /// Copy data to internal structures.
  virtual void InitMesh(vtkPolyData* fromPolydata);
  /// Build connections in internal data structures.
  virtual void BuildConnection();
  /// Copy data from internal structures.
  virtual void DoneMesh(vtkPolyData* toPolydata);
  /// Execution method.
  virtual void Execute() { }

  /// Default constructor.
  VAM_MeshEmptyFilter(void);
  /// Default destructor.
  virtual ~VAM_MeshEmptyFilter(void);
  /// Overridden function, this method does the process.
  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
public:
  /// VTK macro.
  vtkTypeMacro(VAM_MeshEmptyFilter,vtkPolyDataAlgorithm);
  /// Print info about this object.
  void PrintSelf(ostream& os, vtkIndent indent);
  /// Create new instance.
  static VAM_MeshEmptyFilter *New();
};

